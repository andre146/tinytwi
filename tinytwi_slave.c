#ifndef TINYTWI_SLAVE_C
#define TINYTWI_SLAVE_C
#include "tinytwi_slave.h"
#include <util/delay.h>

#define USI_SDA_IN() { DDR_USI &= ~(1<<PORT_USI_SDA); }
#define USI_SDA_OUT() { DDR_USI |= (1<<PORT_USI_SDA); }
#define USI_SDA_HIGH() { PORT_USI |= (1<<PORT_USI_SDA); }
#define USI_SDA_LOW() { PORT_USI &= ~(1<<PORT_USI_SDA); }

#define USI_SCL_IN() { DDR_USI &= ~(1<<PORT_USI_SCL); }
#define USI_SCL_OUT() { DDR_USI |= (1<<PORT_USI_SCL); }
#define USI_SCL_HIGH() { PORT_USI |= (1<<PORT_USI_SCL); }
#define USI_SCL_LOW() { PORT_USI &= ~(1<<PORT_USI_SCL); }

#define USI_SLAVE_CHECK_ADDR 1
#define USI_SLAVE_SEND_DATA 2
#define USI_SLAVE_RECV_CLR_ACK 3
#define USI_SLAVE_SEND_WAIT_ACK 4
#define USI_SLAVE_SEND_CHECK_ACK 5
#define USI_SLAVE_RECV_DATA 6

uint8_t *usi_buffer[USI_BUFFER_SIZE];
uint8_t usi_buffer_offset;
uint8_t usi_buffer_offset_set;

uint8_t usi_slave_addr;
uint8_t usi_slave_state;

void twi_slave_init(uint8_t addr){
    USI_SCL_IN(); //set both pins as inputs and disable pullup resistors
    USI_SCL_LOW();
    USI_SDA_IN();
    USI_SCL_LOW();
    usi_slave_addr = addr; //save the slave's address
    usi_slave_state = 0;
    usi_buffer_offset = 0;
    usi_buffer_offset_set = 0;
    
    USICR = (1<<USISIE) | (1<<USIWM0) | (1<<USIWM1) | (1<<USICS1); //setup USI control register
    USISR = 0;
}

ISR(USI_START_vect){
    usi_slave_state = USI_SLAVE_CHECK_ADDR; //the next step is to check the address 
    USICR |= (1<<USIOIE); //enable USI overflow interrupt
    USISR = 0; //make sure counter is zero
    USI_SDA_IN(); //make sure the SDA pin is an input
    
    while((PIN_USI & (1<<PIN_USI_SCL)) && !(PIN_USI & (1<<PIN_USI_SDA))){} //wait for end of start condition
    
    USISR = (1<<USISIF); //clearing the start condition interrupt flag
}

ISR(USI_OVF_vect){
    switch(usi_slave_state){
        case USI_SLAVE_CHECK_ADDR: 
            if(((USIDR >> 1) == 0) || ((USIDR >> 1) == usi_slave_addr)){ //check if slave is being addressed
                if(USIDR & 1){ //check R/W bit
                    usi_slave_state = USI_SLAVE_SEND_DATA;
                }else{
                    usi_slave_state = USI_SLAVE_RECV_CLR_ACK;
                    usi_buffer_offset_set = 0;
                }
                USIDR = 0; //send acknowledge bit
                USI_SDA_LOW();
                USI_SDA_OUT();
                USISR = 0x0E; //set counter to 14 (dec) to wait one clock pulse (two signal edges)
            }else{
                usi_slave_state = 0; //reset USI
                USICR &= ~(1<<USIOIE);
            }
            break;
            
        case USI_SLAVE_SEND_WAIT_ACK:
            USI_SDA_IN();
            //USI_SDA_LOW();
            USISR = 0x0E;
            usi_slave_state = USI_SLAVE_SEND_CHECK_ACK;
            break;
            
        case USI_SLAVE_SEND_CHECK_ACK:
            if(USIDR & 1){ //no ack
                usi_slave_state = USI_SLAVE_CHECK_ADDR;
                //USICR &= ~(1<<USIOIE);
                break;
            }else{ //ack
                usi_slave_state = USI_SLAVE_SEND_DATA;
            }
            
        case USI_SLAVE_SEND_DATA:
            USI_SDA_HIGH();
            USI_SDA_OUT();
            USIDR = *(usi_buffer[usi_buffer_offset]);
            usi_slave_state = USI_SLAVE_SEND_WAIT_ACK; //next step after sending: wait for ack bit
                        
            if(usi_buffer_offset >= USI_BUFFER_SIZE - 1){
                usi_buffer_offset = 0;
            }else{
                usi_buffer_offset++;
            }
            break;
            
        case USI_SLAVE_RECV_CLR_ACK:
            USI_SDA_IN();
            USI_SDA_LOW();
            usi_slave_state = USI_SLAVE_RECV_DATA;
            break;
            
        case USI_SLAVE_RECV_DATA:
            USI_SDA_IN();
            usi_slave_state = USI_SLAVE_RECV_CLR_ACK;
            
            if(!usi_buffer_offset_set){
               usi_buffer_offset = USIDR;
               usi_buffer_offset_set = 1;
            }else{
                *(usi_buffer[usi_buffer_offset]) = USIDR;
                if(usi_buffer_offset >= USI_BUFFER_SIZE - 1){
                    usi_buffer_offset = 0;
                }else{
                    usi_buffer_offset++;
                }
            }
            USIDR = 0;
            USI_SDA_LOW();
            USI_SDA_OUT();
            USISR = 0x0E;
            break;
    }
    USISR |= (1<<USIOIF); //clear interrupt flag
}
#endif
