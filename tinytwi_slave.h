#ifndef TINYTWI_SLAVE_H
#define TINYTWI_SLAVE_H

#ifndef F_CPU
#define F_CPU 8000000UL
#endif
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

#if defined (__AVR_ATtiny25__) | defined (__AVR_ATtiny45__) | defined (__AVR_ATtiny85__)
	#define DDR_USI		DDRB
	#define PORT_USI	PORTB
	#define PIN_USI		PINB
	#define PORT_USI_SDA    PB0
	#define PORT_USI_SCL    PB2
	#define PIN_USI_SDA	PINB0
	#define PIN_USI_SCL	PINB2
#endif

#define USI_BUFFER_SIZE 20

extern void twi_slave_init(uint8_t addr);

#endif /* TINYTWI_SLAVE_H */

